// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // config: {
  //   apiKey: 'AIzaSyBwGPl4mpTA9Fz2YBwFzDCMnl9wHuhZxJY',
  //   authDomain: 'samplecicd-5dfa7.firebaseapp.com',
  //   databaseURL: 'https://samplecicd-5dfa7.firebaseio.com',
  //   storageBucket: '',
  //   projectID:'samplecicd-5dfa7',
  //   messagingSenderId:'568162553156'
  // }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
